const mongoose = require('mongoose');
    require('mongoose-type-email');
const Schema = mongoose.Schema;
const mongooseFieldEncryption = require("mongoose-field-encryption").fieldEncryption;

const userSchema = new Schema({
    name: { type: String,  required: true},
    email: { type: mongoose.SchemaTypes.Email, lowercase: true, required: true, unique: true },
    userType: { type: String, required: true },
    password: { type: String, required: true},
    phone: { type: String, required: true, unique: true },
    address: { type: String, required: true},
    confirme: { type: Boolean, default: false},
    _token: { type: String, required: true }
});

userSchema.set('toJSON', { virtuals: true });
userSchema.plugin(mongooseFieldEncryption, { fields: ["name", "userType", "phone", "address" ], 
secret: "asd465as4d$adasda!sad546" });

module.exports = mongoose.model('User', userSchema);