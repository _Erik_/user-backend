﻿const config = require('config.json'),
    jwt = require('jsonwebtoken'),
    bcrypt = require('bcryptjs'),
    db = require('_helpers/db'),
    User = db.User,
    nodemailer = require('nodemailer'),
    configEmail = require('../config').emailConfig;

module.exports = {
    authenticate,
    getAll,
    getById,
    create,
    update,
    confirmEmail,
    forgotPassword,
    checkCode,
    restorePassword,
    changePassword,
    editProfile,
    delete: _delete
};

async function authenticate({ email, password }) {
    const user = await User.findOne({ email: email });
    
    if (user && bcrypt.compareSync(password, user.password)) {
        if (user.confirme) {
            const { hash, ...userWithoutHash } = user.toObject();
            const _token = jwt.sign({ sub: user.id }, config.secret);
            user._token = _token;
            await user.save();
            const updateUser = await User.findOne({ email: email });
            return {
                success: true,
                data: {
                    _token: updateUser._token,
                    id: updateUser._id,
                    name: updateUser.name,
                    email: updateUser.email,
                    userType: updateUser.userType,
                    phone: updateUser.phone,
                    address: updateUser.address,
                }
            };
        }

        return {
            message: 'Please confime your email'
        };
    }
    
}

async function getAll() {
    return await User.find();
}

async function getById(id) {
    return await User.findOne({ _id: id });
}

async function create(req) {
    const userParam = req.body;
    const host = req.get('host');

    // validate
    if (await User.findOne({ email: userParam.email })) {
        throw 'Email "' + userParam.email + '" is already taken';
    }

    const user = new User(userParam);

    // hash password
    if (userParam.password) {
        user.password = bcrypt.hashSync(userParam.password, 10);
    }
    
    user._token =jwt.sign({ sub: userParam.username }, config.secret);

    // save user
    await user.save();
    await sendConfirmEmail(userParam.email, user._token, user._id, host);

    return {
        success: true,
        data: {
            message: "Confime Your Email"
        } 
    }
}

async function forgotPassword (req) {
    const userParam = req.body;
    if (userParam.email) {
        const user = await User.findOne({ email: userParam.email });
        if (user) {
            let code = user._token.substring(user._token.length - 8, user._token.length - 4); 
            await sendForgotPasswordEmail (user.email, code);
            return {
                success: true,
                data: {
                    message: 'Please Check Your Email'
                }
            }
        }
    } else {
        throw 'Please Sign Up';
    }

}

async function checkCode (param) {
    if (param.email && param.code) {
        const user = await User.findOne({ email: param.email });
        const tokenCode = user._token.substring(user._token.length - 8, user._token.length - 4).toLowerCase(); 
        if (tokenCode === param.code.toLowerCase()) {
            return {
                success: true,
                data: {
                    token: user._token
                }
            }
        }
    }

}

async function restorePassword (req) {

    if ( req.params.token && req.body.password) {
        const user = await User.findOne({ _token: req.params.token });
        if (req.body.password.length > 5 ) {
            user.password = bcrypt.hashSync(req.body.password, 10);
            user.save()
            return {
                success: true,
                message: "Password was changed"
            }
        }

        throw 'Password lengt must be min 6 ';
 
    }

}

async function confirmEmail(token) {
    const user = await User.findOne({ _token: token });
    if (!user.confirme) {
        user.confirme = true;
        await user.save();
        await sendThanksEmail(user.email);
        
        return {
            success: true,
            message: "Your account is activated"
        }
    }else {
        return {
            success: false,
            message: "Your account already activated"
        }
    }


}

async function changePassword(req) {
    if ( req.params.token && req.body.newPassword && req.body.oldPassword) {
        const user = await User.findOne({ _token: req.params.token });
       if (req.body.oldPassword.length > 5 && req.body.newPassword.length > 5 ) {
            if (user) {
                if (bcrypt.compareSync(req.body.oldPassword, user.password)) {
                    const _token = jwt.sign({ sub: user.id }, config.secret);
                    user.password = bcrypt.hashSync(req.body.newPassword, 10);
                    user._token = _token;
                    
                    await user.save()

                    return {
                        success: true,
                        data: {
                            token: _token
                        }
                    }
                }

                throw 'Old Password is invalid'
            }

       }

       throw 'Password lengt must be min 6';
    }

    throw 'Token, New password and Old Password is required'
}

async function editProfile(req) {
    const token = req.params.token;
    const userParams = req.body;
    const host = req.get('host');

    if (token && userParams) {
        const user = await User.findOne({ _token: req.params.token });

        if (user && bcrypt.compareSync(userParams.password, user.password)) {
            if (userParams.name) user.name = userParams.name;
            if (userParams.email) user.email = userParams.email; user.confirme = false;
            if (userParams.address) user.address = userParams.address;
            if (userParams.phone) user.phone = userParams.phone;
            
            await user.save();
            if (userParams.email) {
                await sendConfirmEmail(userParams.email, user._token, user._id, host);
                return {
                    success: true,
                    data: {
                        message: "Confime Your Email"
                    } 
                }
            }

            return {
                success: true,
            }
        }

        throw 'Email is invalid'
    }
    
    throw 'Password lengt must be min 6';
}

async function update(id, userParam) {
    const user = await User.findById(id);

    // validate
    if (!user) throw 'User not found';
    if (user.username !== userParam.username && await User.findOne({ username: userParam.username })) {
        throw 'Username "' + userParam.username + '" is already taken';
    }

    // hash password if it was entered
    if (userParam.password) {
        userParam.hash = bcrypt.hashSync(userParam.password, 10);
    }

    // copy userParam properties to user
    Object.assign(user, userParam);

    await user.save();

    return user;
}

async function _delete(id, token) {
    const user = await User.findOne({ _id: id});
    if (user._token === token) {
        await User.findByIdAndRemove(id);
        return id;
    } 
}

async function sendConfirmEmail(email, _token, _id, host) {
    
    const transporter = nodemailer.createTransport({
        service: 'Godaddy',
        host: "smtpout.secureserver.net",  
        secureConnection: true,
        port: 465,
        auth: {
            user: "erik@bemyplan.com",
            pass: "Armenia2018!" 
        }
      });
    const link = "http://"+host+"/users/confirm/email?_token="+_token;

    const mailOptions = {
    from: 'erik@bemyplan.com',
    to: email,
    subject: 'Confirm Email',
    html: `<h1><a href="${link}">Click for Confirm</a></h1>`
    };
    
    transporter.sendMail(mailOptions, function(error, info){
        if (error) {
            delete(_id, _token)
            return error;

        }
    });
}

async function sendThanksEmail(email) {
    const transporter = nodemailer.createTransport({
        service: 'Godaddy',
        host: "smtpout.secureserver.net",  
        secureConnection: true,
        port: 465,
        auth: {
            user: "erik@bemyplan.com",
            pass: "Armenia2018!" 
        }
      });
   
    const mailOptions = {
    from: 'erik@bemyplan.com',
    to: email,
    subject: 'Thank you',
    html: `<h1>Your account Acvitated</h1>`
    };
    
    transporter.sendMail(mailOptions, function(error, info){
        if (error) {
            return error;
        }
    });
}

async function sendForgotPasswordEmail(email, code) {
    
    const transporter = nodemailer.createTransport({
        service: 'Godaddy',
        host: "smtpout.secureserver.net",  
        secureConnection: true,
        port: 465,
        auth: {
            user: "erik@bemyplan.com",
            pass: "Armenia2018!" 
        }
      });

    const mailOptions = {
    from: 'erik@bemyplan.com',
    to: email,
    subject: 'Confirmetion Code',
    html: `<h1>${code}</h1>`
    };
    
    transporter.sendMail(mailOptions, function(error, info){
        if (error) {
            delete(_id, _token)
            return error;

        }
    });
}
