﻿const express = require('express');
const router = express.Router();
const userService = require('./user.service');

// routes
router.post('/authenticate', authenticate);
router.post('/register', register);
router.post('/forgot/password', forgotPassword);
router.post('/check/code', checkCode);
router.get('/confirm/email', confirmEmail);
router.post('/restore/password/:token', restorePassword);
router.post('/change/password/:token', changePassword);
router.post('/edit/profile/:token', editProfile);
router.delete('/:id/:token', _delete);
// router.get('/:id', getById);
// router.put('/:id', update);
// router.get('/', getAll);
// router.get('/current', getCurrent);

module.exports = router;

/* 
*   /login
*       paramters
*           name = email,
*           type = string,
*           required = true
*
*           name = password,
*           type = string,
*           required = true
*
*/
function authenticate(req, res, next) {
    userService.authenticate(req.body)
        .then(user => user ? res.json(user) : res.status(400).json({ message: 'Email or password is incorrect' }))
        .catch(err => res.json(err));
}


/* 
*   /register
*       paramters
*           name = name,
*           type = string,
*           required = true
*
*           name = email,
*           type = email,
*           required = true,
*           uniq = true
*
*           name = userType,
*           type = string,
*           value = Private || Commercial,
*           required = true,
*
*           name = password,
*           type = string,
*           required = true,
*
*           name = phone,
*           type = string,
*           required = true,
*           uniq = true
*
*           name = address,
*           type = string,
*           required = true,
*
*/
function register(req, res, next) {
    userService.create(req)
        .then(data => res.json(data))
        .catch(err => next(err));
}

/* 
*   /confirm/email
*       paramters
*           name = token,
*           type = string,
*           required = true
*
*
*/
function confirmEmail(req, res, next) {
    userService.confirmEmail(req.query._token)
        .then(data => res.json(data))
        .catch(err => next(err));
}

function getCurrent(req, res, next) {
    userService.getById(req.user.sub)
        .then(user => user ? res.json(user) : res.sendStatus(404))
        .catch(err => next(err));
}


/* 
*   /confirm/email
*       paramters
*           name = id,
*           type = string,
*           required = true
*
*           name = token,
*           type = string,
*           required = true
*
*
*/
function _delete(req, res, next) {
    userService.delete(req.params.id, req.params.token)
        .then((id) => res.json(id))
        .catch(err => next(err));
}

function forgotPassword(req, res, next) {
    userService.forgotPassword(req)
        .then(data => res.json(data))
        .catch(err => next(err));
}

function checkCode(req, res, next) {
    userService.checkCode(req.body)
        .then(data => res.json(data))
        .catch(err => next(err));
}

function restorePassword(req, res, next) {
    userService.restorePassword(req)
        .then(data => res.json(data))
        .catch(err => next(err));
}

function changePassword(req, res, next) {
    userService.changePassword(req)
        .then(data => res.json(data))
        .catch(err => next(err));
}

function editProfile(req, res, next) {
    userService.editProfile(req)
        .then(data => res.json(data))
        .catch(err => next(err));
}

// function getAll(req, res, next) {
//     userService.getAll()
//         .then(users => res.json(users))
//         .catch(err => next(err));
// }

// function getById(req, res, next) {
//     userService.getById(req.params.id)
//         .then(user => user ? res.json(user) : res.sendStatus(404))
//         .catch(err => next(err));
// }

// function update(req, res, next) {
//     userService.update(req.params.id, req.body)
//         .then(data => res.json(data))
//         .catch(err => next(err));
// }

